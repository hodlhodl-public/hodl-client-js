const path = require('path');
const url = require('url');
const qs = require('qs');
const crypto = require('crypto');

const multisig = require('./multisig');

module.exports = function ({apiKey, apiHost}) {
  const axios = require('axios').create({
    baseURL: url.resolve(apiHost, '/api/v1'),
    headers: {'Authorization': `Bearer ${apiKey}`},
    paramsSerializer: (params) => qs.stringify(params, {encodeValuesOnly: false})
  });

  function genericRequest(request) {
    return request()
      .catch((error) => {
        if (error.response && error.response.headers['content-type'].includes('json')) {
          return Promise.reject(error.response.data);
        } else {
          return Promise.reject(error);
        };
      })
      .then((result) => {
        if (result.data !== '') {
          if (result.data.status === 'success') {
            return result.data;
          } else {
            return Promise.reject(result.data);
          }
        };

        // response code is guaranteed to be 200-299 here,
        // so empty body means everything went well
        return {status: 'success'};
      });
  }

  function genericGet(httpPath, params) {
    return genericRequest(() => axios.get(httpPath, {params}));
  };

  function genericPost(httpPath, payload) {
    return genericRequest(() => axios.post(httpPath, payload));
  };

  function genericPut(httpPath, payload) {
    return genericRequest(() => axios.put(httpPath, payload));
  };

  function genericDelete(httpPath) {
    return genericRequest(() => axios.delete(httpPath));
  };

  return {
    axios: axios,

    crypto: {
      decryptSeed(seed, password) {
        return multisig.decryptSeed(seed, password);
      },

      pubkeyFromDecryptedSeed(seed, index) {
        return multisig.pubkeyFromSeed(seed, index);
      },

      privkeyFromDecryptedSeed(seed, index) {
        return multisig.privkeyFromSeed(seed, index);
      },

      verifyContract({escrow: {address, witness_script, index}}, pubkey) {
        return multisig.verifyDepositAddress(address, witness_script, pubkey);
      },

      signTransaction({hex, in_amounts}, privkey) {
        return multisig.signTransaction(hex, privkey, in_amounts)
      },

      generateHmac(signatureKey, nonce = null) {
        if (nonce === null) {
          nonce = Math.floor(Date.now() / 1000).toString();
        };

        const hmac = crypto.createHmac("sha256", signatureKey).update(`${apiKey}:${nonce}`).digest("hex");

        return {hmac, nonce};
      }
    },

    // contracts

    getContract(id) {
      return genericGet(path.join('/contracts', id));
    },

    createContract(payload, signatureKey = null) {
      if (payload.contract.release_address !== undefined) {
        if (signatureKey === null) {
          return Promise.reject("Provide signatureKey argument to createContract when release_address is present");
        };

        const {hmac, nonce} = this.crypto.generateHmac(signatureKey);

        payload = {...payload, hmac, nonce}
      };

      return genericPost('/contracts', payload);
    },

    confirmContract(id) {
      return genericPost(path.join('/contracts', id, 'confirm'));
    },

    markContractAsPaid(id) {
      return genericPost(path.join('/contracts', id, 'mark_as_paid'));
    },

    cancelContract(id) {
      return genericPost(path.join('/contracts', id, 'cancel'));
    },

    showReleaseTransaction(contractId) {
      return genericGet(path.join('/contracts', contractId, 'release_transaction'));
    },

    showRefundTransaction(contractId, releaseAddress) {
      return genericGet(path.join('/contracts', contractId, 'refund_transaction'), {release_address: releaseAddress});
    },

    signReleaseTransaction(contractId, hex) {
      return genericPost(path.join('/contracts', contractId, 'release_transaction'), {hex});
    },

    signRefundTransaction(contractId, hex) {
      return genericPost(path.join('/contracts', contractId, 'refund_transaction'), {hex});
    },

    signRefundTransaction(contractId, hex) {
      return genericPost(path.join('/contracts', contractId, 'refund_transaction'), {hex});
    },

    startDispute(contractId) {
      return genericPost(path.join('/contracts', contractId, 'dispute'));
    },

    // countries (TODO)

    // currencies (TODO)

    // exchange rate providers (TODO)

    // offers

    getOffer(id) {
      return genericGet(path.join('/offers', id));
    },

    listOffers(params) {
      return genericGet('/offers', params);
    },

    getMyOffers() {
      return genericGet('/offers/my');
    },

    fetchNewOffers(params) {
      return genericGet('/offers/fetch_new', params);
    },

    createOffer(payload) {
      return genericPost('/offers', payload);
    },

    updateOffer(id, payload) {
      return genericPut(path.join('/offers', id), payload);
    },

    deleteOffer(id) {
      return genericDelete(path.join('/offers', id));
    },

    // payment method instructions

    getPaymentMethodInstruction(id) {
      return genericGet(path.join('/payment_method_instructions', id));
    },

    listPaymentMethodInstructions(params) {
      return genericGet('/payment_method_instructions', params);
    },

    createPaymentMethodInstruction(payload) {
      return genericPost('/payment_method_instructions', payload);
    },

    deletePaymentMethodInstruction(id) {
      return genericDelete(path.join('/payment_method_instructions', id));
    },

    // users

    getMyself() {
      return genericGet('/users/me');
    }
  }
};
