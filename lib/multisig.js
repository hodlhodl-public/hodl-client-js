/*
Dependencies:
- sjcl
- bitcore-lib (built from git+https://github.com/braydonf/bitcore-lib.git#segwit)
*/

const bitcore = require("bitcore-lib");
const sjcl = require("sjcl");

module.exports = (function(){

  var CLIENT_SEED_VERSION1 = "ES1";
  var KDFv1Name = "pbkdf2";
  var KDFv1Iterations = 10000;

  var bitsFromHex = function(hex) {
    return sjcl.codec.hex.toBits(hex)
  }
  var hexFromBits = function(bits) {
    return sjcl.codec.hex.fromBits(bits)
  }
  var bitsFromString = function(string) {
    return sjcl.codec.utf8String.toBits(string)
  }
  var stringFromBits = function(bits) {
    return sjcl.codec.utf8String.fromBits(bits)
  }
  var hash160 = function(bits) {
    return sjcl.hash.ripemd160.hash(sjcl.hash.sha256.hash(bits));
  }
  var hash256 = function(bits) {
    return singleHash256(singleHash256(bits));
  }
  var singleHash256 = function(bits) {
    return sjcl.hash.sha256.hash(bits);
  }
  var hash160hex = function(hex) {
    return hexFromBits(hash160(bitsFromHex(hex)));
  }
  var hash256hex = function(hex) {
    return hexFromBits(hash256(bitsFromHex(hex)));
  }
  var singleHash256hex = function(hex) {
    return hexFromBits(singleHash256(bitsFromHex(hex)));
  }

  // password, salt and algorithm are utf-8 strings, iterations is number
  // returns 16-byte AES key as bits array
  var keyBitsFromPassword = function(password, salt, algorithm, iterations) {
    var keyBitlength = 128;
    if (algorithm == KDFv1Name) {
      var keyBits = sjcl.misc.pbkdf2(password, salt, iterations, keyBitlength, sjcl.misc.hmac);
      return keyBits;
    } else {
      return null
    }
  }

  // takes bits, returns bits
  var hmacSha256 = function(keyBits, dataBits) {
    return (new sjcl.misc.hmac(keyBits, sjcl.hash.sha256)).mac(dataBits);
  }

  var fetchTransaction = function(txid) {
    // TODO: fetch tx from external source for a given hex tx id.
  }

  var logInfo = function(message, obj) {
    if (console && console.log) {
      console.log("Multisig Libary: " + message);
      if (obj) {
        console.log(obj);
      }
    }
  }

  var multisigClientAPI = {

    // Creates an unencrypted key by generating random number with sjcl
    // using user supplied password for additional entropy.
    createSeed: function(password) {
      sjcl.random.startCollectors();
      sjcl.random.addEntropy(password, password.length * 3);
      // 512 bits divided by 32-bit words
      // 512 bits is maximum acceptable entropy for bitcore HD keys generation
      var seedBits = sjcl.random.randomWords(512 / 32);
      sjcl.random.stopCollectors();
      return hexFromBits(seedBits);
    },

    // Encrypts the seed using the password.
    // It is designed to be asynchronous in order to support heavier KDF rounds.
    // * seed is hex-encoded seed.
    // * password is a string
    encryptSeed: function(seedHex, password) {

      var kdfName = KDFv1Name;
      var kdfIterations = KDFv1Iterations;

      var seed = bitsFromHex(seedHex);
      var salt = sjcl.bitArray.clamp(hmacSha256(bitsFromString(password), seed), 16*8); // also acts as plaintext MAC
      var aeskey = keyBitsFromPassword(password, salt, kdfName, kdfIterations);
      var iv = sjcl.bitArray.clamp(hmacSha256(aeskey, seed), 12*8); // standard IV length for GCM is 12 bytes.

      var ciphertextWithTag = sjcl.mode.gcm.encrypt(
        new sjcl.cipher.aes(aeskey),
        seed,
        iv,
        null, // aad
        16*8  // tag bitlength
      );

      var resultBits = sjcl.bitArray.concat(iv, ciphertextWithTag);

      return [CLIENT_SEED_VERSION1, // version prefix
              hexFromBits(resultBits),
              hexFromBits(salt),
              kdfName,
              kdfIterations + ""].join(":")
    },

    // Decrypts an encrypted key. Fails if unsupported version is specified or some parameters are invalid.
    // It is designed to be asynchronous in order to support heavier KDF rounds.
    // * password is a string
    // * callback is a function that receives one argument {seed: hex string} or {error: "Unsupported version"}.
    decryptSeed: function(encryptedSeed, password) {
      var esParts = encryptedSeed.split(":")
      var esVersion     = esParts[0]
      var ciphertextHex = esParts[1]
      var saltHex       = esParts[2]
      var kdfName       = esParts[3]
      var kdfIterations = parseInt(esParts[4], 10)

      if (esVersion == CLIENT_SEED_VERSION1) {

        var aeskey = keyBitsFromPassword(password, bitsFromHex(saltHex), kdfName, kdfIterations);
        if (!aeskey) {
          return null;
        }

        var ivct = bitsFromHex(ciphertextHex);
        var iv = sjcl.bitArray.bitSlice(ivct, 0, 12*8);
        var ct = sjcl.bitArray.bitSlice(ivct, 12*8); // contains the tag already
        try {
            var seed = sjcl.mode.gcm.decrypt(
              new sjcl.cipher.aes(aeskey),
              ct,
              iv,
              null, // aad
              16*8  // tag bitlength
            );
            // Verify that seed is correctly decrypted by using salt-as-mac
            var mac = sjcl.bitArray.clamp(hmacSha256(bitsFromString(password), seed), 16*8);

            if (hexFromBits(mac) != saltHex) {
                // Something went wrong during decryption.
                //return "mac does not checkout"
                return null;
            }

            return hexFromBits(seed);
        } catch (e) {
            //return "exception: " + e
            return null;
        }

      } else {
        //return "unknown version"
        return null;
      }
    },

    // Returns base58-encoded extended pubkey from a given seed to send to the server.
    xpubFromSeed: function (seed) {
      return (new bitcore.HDPrivateKey.fromSeed(seed)).hdPublicKey.xpubkey;
    },

    // Returns hex-encoded privkey from a given seed at a given index.
    privkeyFromSeed: function (seed, index) {
      var hdPrivKey = new bitcore.HDPrivateKey.fromSeed(seed)
      return hdPrivKey.derive(index).privateKey.toString();
    },

    // Returns hex-encoded pubkey from a given seed at a given index.
    pubkeyFromSeed: function (seed, index) {
      return new bitcore.PrivateKey(this.privkeyFromSeed(seed, index)).toPublicKey().toString();
    },

    // Verify that p2sh address actually contains m-of-n with user's key.
    // p2sh address is base58-encoded address
    // redeemScript is hex-encoded string.
    // userPubkey is hex-encoded string.
    verifyDepositAddress: function(p2shAddress, redeemScript, userPubkey, asset) {
      if (!p2shAddress || !redeemScript || !userPubkey) {
        return false;
      }
      redeemScript = redeemScript.toLowerCase()
      userPubkey = userPubkey.toLowerCase()

      asset = asset || 'btc';
      var addr = (new bitcore.Address(p2shAddress)).toObject();
      if (addr.type != bitcore.Address.PayToScriptHash) {
        logInfo("Address " + p2shAddress + " is not P2SH.");
        return false;
      }

      var addressHash = addr.hash;
      var redeemScriptHash = hash160hex(redeemScript);

      // 1. Verify that hash within the address matches the redeem script.
      if (addressHash !== redeemScriptHash) {
        // If address is P2WSH-P2SH, it's redeemScript is 'OP_0 <sha256(witnessScript)>'
        // where witnessScript is equal to redeemScript in P2SH address
        var nestedWitnessHash = hash160hex('0020' + singleHash256hex(redeemScript));
        if (addressHash !== nestedWitnessHash) {
          logInfo("Address " + p2shAddress + " contains hash " + addressHash + " while redeemScript's hash is " + redeemScriptHash)
          return false;
        }
      }

      // 2. Now check that redeem script is indeed what we expect: m-of-n multisig with first key belonging to client.
      if (!redeemScript.includes(userPubkey)) {
        logInfo("Redeem script for " + p2shAddress + " does not contain user key " + userPubkey);
        return false;
      }
      var opcodes = ['52', '53', '54', '55', '56', '57', '58', '59', '5A', '5B', '5C', '5D', '5E', '5F', '60']; // opcodes from OP_2 to OP_16
      var opStr = '(' + opcodes.join('|') + ')';
      var pushdata33 = '21'
      var opMultisig = 'ae'
      var regexpString = "^" + opStr + "(" + pushdata33 + "(" + userPubkey + "|[0-9a-f]{66}))+" + opStr + opMultisig + "$"; // 66 = 2*33
      if (!redeemScript.match(new RegExp(regexpString))) {
        logInfo("Redeem script for " + p2shAddress + " is not of form m-of-n multisig with one of keys belonging to user: " + redeemScript + "; user key: " + userPubkey)
        return false;
      }
      return true;
    },

    // Returns signed tx hex string where first input is m-of-n multisig co-signed with a given privkey.
    // Hash for witness signature requires input's utxo staoshis value. To avoid connecting to bitcoin network from
    // current function we receive 'satoshis' parameter which is array, containing utxo value for input with the same index
    // For example, we have two inputs: [ { utxo: 20000 }, { utxo: 10000 } ], then satoshis array should be [20000, 10000]
    signTransaction: function(txhex, hex_privkeys, satoshis, sigindex) {
      if (typeof(satoshis) === 'undefined')
        satoshis = [];

      if (!(satoshis.constructor === Array))
        satoshis = [satoshis];

      if (typeof(sigindex) === 'undefined')
        sigindex = 2

      if (!(hex_privkeys.constructor === Array))
        hex_privkeys = [hex_privkeys];

      var tx = new bitcore.Transaction(txhex);

      var privkeys = hex_privkeys.map(key => new bitcore.PrivateKey(key));

      for (var i = 0; i < tx.inputs.length; i++) {
        var input = tx.inputs[i];
        if (input.hasWitnesses()) {
          var witnesses = input.getWitnesses();
          var witnessScript = bitcore.Script.fromBuffer(witnesses[3]);
          var scriptCodeWriter = new bitcore.encoding.BufferWriter();
          var witnessScriptBuffer = witnessScript.toBuffer();
          scriptCodeWriter.writeVarintNum(witnessScriptBuffer.length);
          scriptCodeWriter.write(witnessScriptBuffer);
          var satoshisBN = bitcore.crypto.BN.fromNumber(satoshis[i]);
          var satoshisBuffer = new bitcore.encoding.BufferWriter().writeUInt64LEBN(satoshisBN).toBuffer();
          var signature = bitcore.Transaction.SighashWitness.sign(tx, privkeys[i], bitcore.crypto.Signature.SIGHASH_ALL, i, scriptCodeWriter.toBuffer(), satoshisBuffer);
          signature.nhashtype = bitcore.crypto.Signature.SIGHASH_ALL;
          witnesses[sigindex] = signature.toTxFormat();
          input.setWitnesses(witnesses);
        } else {
          var redeemscript = bitcore.Script.fromBuffer(input.script.chunks[3].buf); // [OP_0 sig1 sig2 redeemscript]
          var signature = bitcore.Transaction.sighash.sign(tx, privkeys[i], bitcore.crypto.Signature.SIGHASH_ALL, i, redeemscript);
          signature.nhashtype = bitcore.crypto.Signature.SIGHASH_ALL;

          var newchunks = [bitcore.Opcode.OP_0];
          newchunks[sigindex] = signature.toTxFormat();
          for (var i = 1; i < input.script.chunks.length; i++) {
            if (i != sigindex)
              newchunks[i] = input.script.chunks[i].buf;
          }

          var newscript = bitcore.Script();
          for (var chunk of newchunks) {
            newscript.add(chunk);
          }
          input.setScript(newscript);
        }
      }
      return tx.toString();
    },
  };

  /** @fileOverview Javascript RIPEMD-160 implementation.
   *
   * @author Artem S Vybornov <vybornov@gmail.com>
   */
  (function() {

  /**
   * Context for a RIPEMD-160 operation in progress.
   * @constructor
   * @class RIPEMD, 160 bits.
   */
  sjcl.hash.ripemd160 = function (hash) {
      if (hash) {
          this._h = hash._h.slice(0);
          this._buffer = hash._buffer.slice(0);
          this._length = hash._length;
      } else {
          this.reset();
      }
  };

  /**
   * Hash a string or an array of words.
   * @static
   * @param {bitArray|String} data the data to hash.
   * @return {bitArray} The hash value, an array of 5 big-endian words.
   */
  sjcl.hash.ripemd160.hash = function (data) {
    return (new sjcl.hash.ripemd160()).update(data).finalize();
  };

  sjcl.hash.ripemd160.prototype = {
      /**
       * Reset the hash state.
       * @return this
       */
      reset: function () {
          this._h = _h0.slice(0);
          this._buffer = [];
          this._length = 0;
          return this;
      },

      /**
       * Reset the hash state.
       * @param {bitArray|String} data the data to hash.
       * @return this
       */
      update: function (data) {
          if ( typeof data === "string" )
              data = sjcl.codec.utf8String.toBits(data);

          var i, b = this._buffer = sjcl.bitArray.concat(this._buffer, data),
              ol = this._length,
              nl = this._length = ol + sjcl.bitArray.bitLength(data);
          for (i = 512+ol & -512; i <= nl; i+= 512) {
              var words = b.splice(0,16);
              for ( var w = 0; w < 16; ++w )
                  words[w] = _cvt(words[w]);

              _block.call( this, words );
          }

          return this;
      },

      /**
       * Complete hashing and output the hash value.
       * @return {bitArray} The hash value, an array of 5 big-endian words.
       */
      finalize: function () {
          var b = sjcl.bitArray.concat( this._buffer, [ sjcl.bitArray.partial(1,1) ] ),
              l = ( this._length + 1 ) % 512,
              z = ( l > 448 ? 512 : 448 ) - l % 448,
              zp = z % 32;

          if ( zp > 0 )
              b = sjcl.bitArray.concat( b, [ sjcl.bitArray.partial(zp,0) ] )
          for ( ; z >= 32; z -= 32 )
              b.push(0);

          b.push( _cvt( this._length | 0 ) );
          b.push( _cvt( Math.floor(this._length / 0x100000000) ) );

          while ( b.length ) {
              var words = b.splice(0,16);
              for ( var w = 0; w < 16; ++w )
                  words[w] = _cvt(words[w]);

              _block.call( this, words );
          }

          var h = this._h;
          this.reset();

          for ( var w = 0; w < 5; ++w )
              h[w] = _cvt(h[w]);

          return h;
      }
  };

  var _h0 = [ 0x67452301, 0xefcdab89, 0x98badcfe, 0x10325476, 0xc3d2e1f0 ];

  var _k1 = [ 0x00000000, 0x5a827999, 0x6ed9eba1, 0x8f1bbcdc, 0xa953fd4e ];
  var _k2 = [ 0x50a28be6, 0x5c4dd124, 0x6d703ef3, 0x7a6d76e9, 0x00000000 ];
  for ( var i = 4; i >= 0; --i ) {
      for ( var j = 1; j < 16; ++j ) {
          _k1.splice(i,0,_k1[i]);
          _k2.splice(i,0,_k2[i]);
      }
  }

  var _r1 = [  0,  1,  2,  3,  4,  5,  6,  7,  8,  9, 10, 11, 12, 13, 14, 15,
               7,  4, 13,  1, 10,  6, 15,  3, 12,  0,  9,  5,  2, 14, 11,  8,
               3, 10, 14,  4,  9, 15,  8,  1,  2,  7,  0,  6, 13, 11,  5, 12,
               1,  9, 11, 10,  0,  8, 12,  4, 13,  3,  7, 15, 14,  5,  6,  2,
               4,  0,  5,  9,  7, 12,  2, 10, 14,  1,  3,  8, 11,  6, 15, 13 ];
  var _r2 = [  5, 14,  7,  0,  9,  2, 11,  4, 13,  6, 15,  8,  1, 10,  3, 12,
               6, 11,  3,  7,  0, 13,  5, 10, 14, 15,  8, 12,  4,  9,  1,  2,
              15,  5,  1,  3,  7, 14,  6,  9, 11,  8, 12,  2, 10,  0,  4, 13,
               8,  6,  4,  1,  3, 11, 15,  0,  5, 12,  2, 13,  9,  7, 10, 14,
              12, 15, 10,  4,  1,  5,  8,  7,  6,  2, 13, 14,  0,  3,  9, 11 ];

  var _s1 = [ 11, 14, 15, 12,  5,  8,  7,  9, 11, 13, 14, 15,  6,  7,  9,  8,
               7,  6,  8, 13, 11,  9,  7, 15,  7, 12, 15,  9, 11,  7, 13, 12,
              11, 13,  6,  7, 14,  9, 13, 15, 14,  8, 13,  6,  5, 12,  7,  5,
              11, 12, 14, 15, 14, 15,  9,  8,  9, 14,  5,  6,  8,  6,  5, 12,
               9, 15,  5, 11,  6,  8, 13, 12,  5, 12, 13, 14, 11,  8,  5,  6 ];
  var _s2 = [  8,  9,  9, 11, 13, 15, 15,  5,  7,  7,  8, 11, 14, 14, 12,  6,
               9, 13, 15,  7, 12,  8,  9, 11,  7,  7, 12,  7,  6, 15, 13, 11,
               9,  7, 15, 11,  8,  6,  6, 14, 12, 13,  5, 14, 13, 13,  7,  5,
              15,  5,  8, 11, 14, 14,  6, 14,  6,  9, 12,  9, 12,  5, 15,  8,
               8,  5, 12,  9, 12,  5, 14,  6,  8, 13,  6,  5, 15, 13, 11, 11 ];

  function _f0(x,y,z) {
      return x ^ y ^ z;
  };

  function _f1(x,y,z) {
      return (x & y) | (~x & z);
  };

  function _f2(x,y,z) {
      return (x | ~y) ^ z;
  };

  function _f3(x,y,z) {
      return (x & z) | (y & ~z);
  };

  function _f4(x,y,z) {
      return x ^ (y | ~z);
  };

  function _rol(n,l) {
      return (n << l) | (n >>> (32-l));
  }

  function _cvt(n) {
      return ( (n & 0xff <<  0) <<  24 )
           | ( (n & 0xff <<  8) <<   8 )
           | ( (n & 0xff << 16) >>>  8 )
           | ( (n & 0xff << 24) >>> 24 );
  }

  function _block(X) {
      var A1 = this._h[0], B1 = this._h[1], C1 = this._h[2], D1 = this._h[3], E1 = this._h[4],
          A2 = this._h[0], B2 = this._h[1], C2 = this._h[2], D2 = this._h[3], E2 = this._h[4];

      var j = 0, T;

      for ( ; j < 16; ++j ) {
          T = _rol( A1 + _f0(B1,C1,D1) + X[_r1[j]] + _k1[j], _s1[j] ) + E1;
          A1 = E1; E1 = D1; D1 = _rol(C1,10); C1 = B1; B1 = T;
          T = _rol( A2 + _f4(B2,C2,D2) + X[_r2[j]] + _k2[j], _s2[j] ) + E2;
          A2 = E2; E2 = D2; D2 = _rol(C2,10); C2 = B2; B2 = T; }
      for ( ; j < 32; ++j ) {
          T = _rol( A1 + _f1(B1,C1,D1) + X[_r1[j]] + _k1[j], _s1[j] ) + E1;
          A1 = E1; E1 = D1; D1 = _rol(C1,10); C1 = B1; B1 = T;
          T = _rol( A2 + _f3(B2,C2,D2) + X[_r2[j]] + _k2[j], _s2[j] ) + E2;
          A2 = E2; E2 = D2; D2 = _rol(C2,10); C2 = B2; B2 = T; }
      for ( ; j < 48; ++j ) {
          T = _rol( A1 + _f2(B1,C1,D1) + X[_r1[j]] + _k1[j], _s1[j] ) + E1;
          A1 = E1; E1 = D1; D1 = _rol(C1,10); C1 = B1; B1 = T;
          T = _rol( A2 + _f2(B2,C2,D2) + X[_r2[j]] + _k2[j], _s2[j] ) + E2;
          A2 = E2; E2 = D2; D2 = _rol(C2,10); C2 = B2; B2 = T; }
      for ( ; j < 64; ++j ) {
          T = _rol( A1 + _f3(B1,C1,D1) + X[_r1[j]] + _k1[j], _s1[j] ) + E1;
          A1 = E1; E1 = D1; D1 = _rol(C1,10); C1 = B1; B1 = T;
          T = _rol( A2 + _f1(B2,C2,D2) + X[_r2[j]] + _k2[j], _s2[j] ) + E2;
          A2 = E2; E2 = D2; D2 = _rol(C2,10); C2 = B2; B2 = T; }
      for ( ; j < 80; ++j ) {
          T = _rol( A1 + _f4(B1,C1,D1) + X[_r1[j]] + _k1[j], _s1[j] ) + E1;
          A1 = E1; E1 = D1; D1 = _rol(C1,10); C1 = B1; B1 = T;
          T = _rol( A2 + _f0(B2,C2,D2) + X[_r2[j]] + _k2[j], _s2[j] ) + E2;
          A2 = E2; E2 = D2; D2 = _rol(C2,10); C2 = B2; B2 = T; }

      T = this._h[1] + C1 + D2;
      this._h[1] = this._h[2] + D1 + E2;
      this._h[2] = this._h[3] + E1 + A2;
      this._h[3] = this._h[4] + A1 + B2;
      this._h[4] = this._h[0] + B1 + C2;
      this._h[0] = T;
  }

  })();


  return multisigClientAPI;
})();
