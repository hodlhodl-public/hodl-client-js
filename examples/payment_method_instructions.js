const util = require("util");
const inspect = (o) => util.inspect(o, false, null, true);

const hodl = require("./config");

async function run () {
  console.log("Creating payment method instruction\n");
  result = hodl.createPaymentMethodInstruction({payment_method_instruction: {
    payment_method_id: 1,
    name: `Test instruction (${Date.now()})`,
    details: `Send money to account #123`
  }});
  result = await result.then(
    function(success) { console.log(inspect(success)); return success; },
    function(error) { console.log("error:"); console.log(inspect(error)) }
  );
  const paymentMethodInstructionID = result.payment_method_instruction.id;
  console.log(`Created payment method instruction with ID = ${paymentMethodInstructionID}`);
  console.log("\n");

  console.log("Getting payment method instruction\n");
  result = hodl.getPaymentMethodInstruction(paymentMethodInstructionID);
  await result.then(
    function(success) { console.log(success); return success; },
    function(error) { console.log("error:"); console.log(error) }
  );
  console.log("\n");

  console.log("Listing payment method instructions\n");
  result = hodl.listPaymentMethodInstructions();
  await result.then(
    function(success) { console.log(success); return success; },
    function(error) { console.log("error:"); console.log(error) }
  );
  console.log("\n");

  console.log("Deleting payment method instruction\n");
  result = hodl.deletePaymentMethodInstruction(paymentMethodInstructionID);
  await result.then(
    function(success) { console.log(success); return success; },
    function(error) { console.log("error:"); console.log(error) }
  );
  console.log("\n");
};

run();
