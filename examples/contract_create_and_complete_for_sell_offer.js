const util = require("util");
const inspect = (o) => util.inspect(o, false, null, true);

const hodl = require("./config");
const offerId = hodl.exampleSellOfferId;
const paymentPassword = hodl.paymentPassword;

const keypress = async () => {
  process.stdin.setRawMode(true);
  return new Promise(resolve => process.stdin.once("data", () => {
    process.stdin.setRawMode(false);
    resolve();
  }))
}

async function run() {
  console.log("Getting encrypted seed\n");
  let result = hodl.getMyself();
  result = await result.then(
    function(success) { console.log(success); return success; },
    function(error) { console.log("error:"); console.log(error) }
  );

  const encryptedSeed = result.user.encrypted_seed;
  console.log("Encrypted seed:", encryptedSeed);
  const decryptedSeed = hodl.crypto.decryptSeed(encryptedSeed, paymentPassword);
  console.log("Decrypted seed:", decryptedSeed);
  console.log("\n");

  console.log("Getting counterparty's offer\n");
  result = hodl.getOffer(offerId);
  result = await result.then(
    function(success) { console.log(success); return success; },
    function(error) { console.log("error:"); console.log(error) }
  );
  console.log("\n");

  const offer = result.offer;
  const offerVersion = offer.version;
  const pmiId = offer.payment_method_instructions[0].id;
  const pmiVersion = offer.payment_method_instructions[0].version;

  console.log("Creating contract\n");
  result = hodl.createContract({contract: {
    offer_id: offerId,
    offer_version: offerVersion,
    payment_method_instruction_id: pmiId,
    payment_method_instruction_version: pmiVersion,
    value: 11
  }});
  result = await result.then(
    function(success) { console.log(inspect(success)); return success; },
    function(error) { console.log("error:"); console.log(inspect(error)) }
  );
  let contract = result.contract;
  console.log("\n");

  while (contract.escrow.address === null) {
    console.log(`Waiting for the counterparty to generate escrow (press any key to check)`);
    await keypress();

    console.log("Checking contract");
    result = await hodl.getContract(contract.id).then(
      function(success) { console.log(success); return success; },
      function(error) { console.log("error:"); console.log(error) }
    );

    contract = result.contract;
  }

  console.log("Veryfing escrow address\n");
  console.log("Public key index:", contract.escrow.index);
  const pubkey = hodl.crypto.pubkeyFromDecryptedSeed(decryptedSeed, contract.escrow.index);
  console.log("Public key:", pubkey);
  if (hodl.crypto.verifyContract(contract, pubkey) === true) {
    console.log("Success");
  } else {
    console.log("Failure");
    return;
  }
  console.log("\n");

  console.log("Confirming contract\n");
  result = hodl.confirmContract(contract.id);
  result = await result.then(
    function(success) { console.log(inspect(success)); return success; },
    function(error) { console.log("error:"); console.log(inspect(error)) }
  );
  console.log("\n");

  while (contract.status === "pending") {
    console.log(`Waiting for the seller to confirm contract (press any key to check)`);
    await keypress();

    console.log("Checking contract status");
    result = await hodl.getContract(contract.id).then(
      function(success) { console.log(success); return success; },
      function(error) { console.log("error:"); console.log(error) }
    );

    contract = result.contract;
  }

  while (contract.status === "depositing") {
    console.log(`Waiting for seller to deposit ${contract.volume} BTC now to ${contract.escrow.address} (press any key)`);
    await keypress();

    console.log("Checking contract status");
    result = await hodl.getContract(contract.id).then(
      function(success) { console.log(success); return success; },
      function(error) { console.log("error:"); console.log(error) }
    );

    contract = result.contract;
  }

  console.log("Marking contract as paid\n");
  result = hodl.markContractAsPaid(contract.id);
  result = await result.then(
    function(success) { console.log(inspect(success)); return success; },
    function(error) { console.log("error:"); console.log(inspect(error)) }
  );
  console.log("\n");

  console.log("Done here. Seller should finalize the contract/release the escrow. Press Ctrl+C to exit.");
}

run();
