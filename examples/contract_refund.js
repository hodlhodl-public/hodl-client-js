const util = require("util");
const inspect = (o) => util.inspect(o, false, null, true);

const hodl = require("./config");
const paymentPassword = hodl.paymentPassword;

const readline = require("readline");
function askQuestion(query) {
  const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });

  return new Promise(resolve => rl.question(query, ans => {
    rl.close();
    resolve(ans);
  }))
};

async function run() {
  console.log("Getting encrypted seed\n");
  let result = hodl.getMyself();
  result = await result.then(
    function(success) { console.log(inspect(success)); return success; },
    function(error) { console.log("error:"); console.log(inspect(error)) }
  );

  const encryptedSeed = result.user.encrypted_seed;
  console.log("Encrypted seed:", encryptedSeed);
  const decryptedSeed = hodl.crypto.decryptSeed(encryptedSeed, paymentPassword);
  console.log("Decrypted seed:", decryptedSeed);
  console.log("\n");

  const contractId = await askQuestion(
    "Enter contract ID, where you are the seller and \
    you won the dispute or the contract has been canceled"
  );
  console.log("Contract ID:", contractId);

  console.log("Donwloading contract data\n");
  result = hodl.getContract(contractId);
  result = await result.then(
    function(success) { console.log(inspect(success)); return success; },
    function(error) { console.log("error:"); console.log(inspect(error)) }
  );
  let contract = result.contract;
  console.log("\n");

  const releaseAddress = await askQuestion("Enter release address: ");
  console.log("Release address:", releaseAddress);

  console.log("Downloading refund transaction\n");
  result = hodl.showRefundTransaction(contractId, releaseAddress);
  result = await result.then(
    function(success) { console.log(inspect(success)); return success; },
    function(error) { console.log("error:"); console.log(inspect(error)) }
  );
  const releaseTransactionData = result.transaction;
  console.log("\n");

  console.log("Signing release transaction on the client\n");
  const privkey = hodl.crypto.privkeyFromDecryptedSeed(decryptedSeed, contract.escrow.index);
  console.log("Privkey:", privkey);
  const signedTransactionHex = hodl.crypto.signTransaction(releaseTransactionData, privkey);
  console.log("\n");

  console.log("Sending signed transaction to the server\n");
  result = hodl.signRefundTransaction(contractId, signedTransactionHex);
  result = await result.then(
    function(success) { console.log(inspect(success)); return success; },
    function(error) { console.log("error:"); console.log(inspect(error)) }
  );
  console.log("\n");
};

run();
